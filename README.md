# cj-movies-exercise

My attempt at the IT-Recruiting exercise.

```
 _(collections)
  .keyBy('id') // or .indexBy() if using lodash 3.x
  .at(ids)
  .value();
```

## Helpful Console Logs
```
console.log('intersectionWith: ', this.nickAndKeanuMovies);

console.log('grab names: ', this.nickAndKeanuMoviesWithNames);

console.log('actors: ', this.actors);

console.log('Actors with Nic: ', this.nickMovieActors);
console.log('Actors with Keanu: ', this.keanuMovieActors);

console.log('Nick ID: ', _.map(this.nickCage, 'actorId'));
console.log('Keanu ID: ', _.map(this.keanuReeves, 'actorId'));
```