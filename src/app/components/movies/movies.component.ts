import {Component, OnInit} from '@angular/core';
import {MoviesService, Movie} from '../../services/movies.service';
import {ActorsService, Actor} from '../../services/actors.service';
import {ActorsListService, ActorsList} from '../../services/actors-list.service';
import * as _ from 'lodash';
import {Observable, forkJoin} from 'rxjs';
import 'rxjs/add/operator/map';

@Component({selector: 'app-movies', templateUrl: './movies.component.html', styleUrls: ['./movies.component.scss']})

export class MoviesComponent implements OnInit {
    movies: Movie[] = [];
    actors: Actor[] = [];
    nickCage: Actor[];
    nickCageMovies: Movie[];
    keanuReevesMovies: Movie[];
    nickId: number;
    keanuReeves: Actor[];
    keanuId: number;
    nickAndKeanuMovieActors: number[];
    nickAndKeanuMoviesWithNames: Actor[];
    nickMovieActors: number[];
    keanuMovieActors: number[];
    movieColumns: string[] = ["title", "movieId"];
    bothMovieColumns: string[] = ["name", "actorId"];
    actorsBothMoviesColumns: string[] = ["name", "KRMovies", "NCMovies"];
    resultObject: ActorsList[] = [];
    displayResult: ActorsList[] = [];

    constructor(private _moviesService: MoviesService, private _actorsService: ActorsService, private _actorsListService: ActorsListService) {}

    ngOnInit() {

        forkJoin([
            this.getMovies(),
            this.getActors()
        ]).subscribe(results => {
            this.nickId = _.map(this.nickCage, 'actorId')[0];
            this.keanuId = _.map(this.keanuReeves, 'actorId')[0];
            this.nickCageMovies = _.filter(this.movies, movie => movie.actors.indexOf(this.nickId) > -1);
            console.log("nickCageMovies: ", this.nickCageMovies);
            this.keanuReevesMovies = _.filter(this.movies, movie => movie.actors.indexOf(this.keanuId) > -1);

            this.nickMovieActors = _.pull(_.uniq(_.flatMap(_.map(this.nickCageMovies, 'actors'))), this.nickId);
            this.keanuMovieActors = _.pull(_.uniq(_.flatMap(_.map(this.keanuReevesMovies, 'actors'))), this.keanuId);
            this.nickAndKeanuMovieActors = _.intersectionWith(this.nickMovieActors, this.keanuMovieActors, _.isEqual);
            this.nickAndKeanuMoviesWithNames = _(this.actors)
                .keyBy('actorId')
                .at(this.nickAndKeanuMovieActors)
                .value();

            console.log('nickAndKeanuMoviesWithNames: ', this.nickAndKeanuMoviesWithNames);
            console.log("nickCageMovies: ", this.nickCageMovies);
            console.log("keanuReevesMovies: ", this.keanuReevesMovies);

            this
                .nickAndKeanuMoviesWithNames
                .forEach(({actorId, name}) => {
                    const result = {
                        name: name,
                        NCMovies: [],
                        KRMovies: []
                    };
                    console.log("the Actor Id: ", actorId);
                    this
                        .nickCageMovies
                        .forEach(movie => {
                            if (movie.actors) {
                                if (movie.actors.indexOf(actorId) > -1) {
                                    // console.log("here's a NMovie one: ", movie);
                                    result
                                        .NCMovies
                                        .push(movie.title);
                                }
                            } else {
                                console.log("issue with NMovie: ", movie);
                            }
                        });
                    this
                        .keanuReevesMovies
                        .forEach(movie => {
                            if (movie.actors) {
                                if (movie.actors.indexOf(actorId) > -1) {
                                    // console.log("here's a KMovie one: ", movie);
                                    result
                                        .KRMovies
                                        .push(movie.title);
                                }
                            } else {
                                console.log("issue with KMovie: ", movie);
                            }
                        });
                    this
                        .resultObject
                        .push(result);
                        console.log("here's the result object: ", this.resultObject);
                    
                });
            this.displayResult = this.resultObject;
            console.log("Display what: ", this.displayResult);
            // To-do: Figure out posting this
            this
                ._actorsListService
                .addResults(this.resultObject)
                .subscribe(serverResponse => console.log("Status Code: ", serverResponse.status));

        });
    }

    getMovies(): Observable < any > {
        return this
            ._moviesService
            .getMovies()
            .map(movieData => {
                this.movies = movieData;
                console.log("where's the movies, bro? ", this.movies);
                return movieData;
            });
    }
    getActors(): Observable < any > {
        return this
            ._actorsService
            .getActors()
            .map(actorData => {
                this.actors = actorData;
                console.log("Where's the actors, bro? ", this.actors);
                this.nickCage = _.filter(this.actors, ['name', 'Nicolas Cage']);
                console.log("Nick the actor? ", this.nickCage);
                this.keanuReeves = _.filter(this.actors, ['name', 'Keanu Reeves']);
                console.log("Keanu the actor? ", this.keanuReeves);
                return actorData;
            });
    }
}
