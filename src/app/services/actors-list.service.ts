import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {HttpHeaders} from '@angular/common/http';

export interface ActorsList {
    name: string;
    KRMovies: string[];
    NCMovies: string[];
}

// tslint:disable-next-line:no-empty-interface
export interface ValidationResponse {
}

@Injectable({providedIn: 'root'})

export class ActorsListService {

    private _url = "https://ceamovies.azurewebsites.net/api/validation/";

    constructor(private http: HttpClient) {}

    addResults(actorsList: ActorsList[]): Observable<HttpResponse<ValidationResponse>> {
        const headers = new HttpHeaders().set('x-chmura-cors', '3df06335-f74f-4e1f-b32d-ff2909b5241b');
        return this.http.post<HttpResponse<ValidationResponse>>(this._url, actorsList, {headers: headers, observe: 'response'});
    }
}
