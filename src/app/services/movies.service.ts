import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders } from '@angular/common/http';

export interface Movie {
    movieId: number;
    title: string;
    actors: number[];
}

@Injectable()
export class MoviesService {
    
    private _url = "https://ceamovies.azurewebsites.net/api/movies/"

    constructor(private http: HttpClient) {}

    getMovies(): Observable<Movie[]> {
        const headers = new HttpHeaders().set('x-chmura-cors', '3df06335-f74f-4e1f-b32d-ff2909b5241b');
        console.log("here's the Movies url: ", this.http.get(this._url, {headers: headers}));
        return this.http.get<Movie[]>(this._url, {headers: headers});
    }
}
