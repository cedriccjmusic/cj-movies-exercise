import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders } from '@angular/common/http';

export interface Actor {
    actorId: number;
    name: string;
}

@Injectable()
export class ActorsService {
    
    private _url = "https://ceamovies.azurewebsites.net/api/actors/"

    constructor(private http: HttpClient) {}

    getActors(): Observable<Actor[]> {
        const headers = new HttpHeaders().set('x-chmura-cors', '3df06335-f74f-4e1f-b32d-ff2909b5241b');
        console.log("here's the Actors url: ", this.http.get(this._url, {headers: headers}));
        return this.http.get<Actor[]>(this._url, {headers: headers});
    }
}
